﻿using System;
using System.Text;

namespace ConsoleApp1_Html_Parser
{
    class Article
    {
        public int Id { get; set; }
        public String Tittle { get; set; }
        public DateTime Date { get; set; }
        public String Info { get; set; }
        public byte[] Img { get; set; }
        public String Full { get; set; }

        public override string ToString()
        {
            return string.Format("id:\n{0}\n", Id) +
                   string.Format("Tittle:\n{0}\n", Tittle) +
                   string.Format("Date:\n{0}\n", Date) +
                   string.Format("Info:\n{0}\n", Info) +
                   string.Format("Full:\n{0}\n", Full) +
            "imgBytes:\n" + Encoding.Default.GetString(Img).Substring(0, 10) + "...";
        }

      /*  private static Object syncLock = new Object();
        private static NewsContext db = new NewsContext();


        public static void ParseArticle()
        {
            var files = Directory.GetFiles(pathArticle);
            Parallel.For(0, files.Length, new ParallelOptions { MaxDegreeOfParallelism = 1 }, (i, status) => {
                var html = File.ReadAllText(files[i]);
                Article article = null;
                try
                {
                    article = SelectModelArticle(html);
                    if (article != null)
                    {
                        lock (syncLock)
                        {
                            db.Add(article);
                            db.SaveChanges();
                            File.Delete(files[i]);
                        }
                    }
                }
                catch (DbUpdateException ex)
                {
                    lock (syncLock)
                    {
                        db = new NewsContext();
                    }
                    Console.WriteLine(ex.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            });
        }


        private static Article SelectModelArticle(string html)
        {
            Article item = new Article();

            var parser = new HtmlParser();
            var document = parser.Parse(html);
            Console.WriteLine("parse html ok");
            try
            {
                using (MyWebClient client = new MyWebClient())
                {
                    item.Tittle = document.QuerySelector("#content > div > div.bb.js-mediator-articles > h1").TextContent.Trim();
                    Console.WriteLine("tittle ok");
                    item.Date = DateTime.Parse(document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.date").TextContent.Trim().Split("\n")[0]);
                    Console.WriteLine("date ok");
                    item.Img = client.DownloadData(document.QuerySelector("img.thumb").GetAttribute("src"));
                    Console.WriteLine("img ok");
                    try
                    {
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.date").Remove();
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > h1").Remove();
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.noprint").Remove();
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.text.clearfix").Remove();
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.js-tags-for-matherial").Remove();
                        document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.clr").Remove();
                    }
                    catch (Exception ex)
                    {
                    }
                    var image = document.QuerySelector("div.photo-vip");
                    if (image == null)
                        image = document.QuerySelector("#content > div > div.bb.js-mediator-articles > div.item-news-canvas__wrapper-img");
                    image.Remove();

                    var info = document.QuerySelector("#content > div > div.bb.js-mediator-articles").TextContent.Trim();
                    if (info.Length > 200)
                        item.Info = info.Substring(0, 200) + "...";
                    else
                        item.Info = info;
                    Console.WriteLine("info ok");
                    // item.Full


                    /*         var pic = document.QuerySelector("img.thumb");
                             pic.SetAttribute("src", $"data:image/jpeg;base64,{Convert.ToBase64String(client.DownloadData(pic.GetAttribute("src")))}");
                             

                    var pics = document.QuerySelectorAll("#photo > li > div > noscript");

                    var imgUrls = new List<String>();
                    foreach (var p in pics)
                    {
                        var img = p.InnerHtml.Trim();
                        var src = img.Substring(10, img.Length - 19);
                        imgUrls.Add(src);
                    }
                    try
                    {
                        document.QuerySelector("div.gamma-container").Remove();
                    }
                    catch (Exception ex)
                    {
                    }
                    item.Full = document.QuerySelector("div.html-format").InnerHtml.Trim();

                    if (item.Full == null)
                    {
                        return null;
                    }
                    Console.WriteLine("full ok");
                    if (imgUrls.Count > 0)
                    {
                        item.Full += "\n<div class=\"row col-md-6\">";
                    }
                    for (int i = 0; i < imgUrls.Count; i++)
                    {
                        item.Full += $"\n<img style=\"width: 100 %; \" src=\"{Convert.ToBase64String(client.DownloadData(imgUrls[i]))}\"/>\n";
                    }
                    if (imgUrls.Count > 0)
                    {
                        item.Full += "</div>";
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
            Console.WriteLine("model ok");
            return item;
        }*/
    }
}

﻿using System.Data.Entity;

namespace ConsoleApp1_Html_Parser
{
    class NewsContext:DbContext
    {
        public NewsContext() : base("mysqlCon")
        {
        }

        public DbSet<News> news { get; set; }
        public DbSet<Article> articles { get; set; }

    }
}

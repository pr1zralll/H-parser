﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1_Html_Parser
{
    class MyWebClient : WebClient
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            var w = (HttpWebRequest)base.GetWebRequest(uri);
            w.Timeout = 15 * 1000;
            return w;
        }
        public string ContentType;

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = base.GetWebResponse(request);
            if (response != null)
            {
                this.ContentType = response.Headers["Content-Type"];
            }
            return response;
        }
    }
}

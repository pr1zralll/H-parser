﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Text;
using CsQuery;

namespace ConsoleApp1_Html_Parser
{
    class News
    {
        public int Id { get; set; }
        public String Tittle { get; set; }
        public DateTime Date { get; set; }
        public String Info { get; set; }
        public byte[] Img { get; set; }
        public String Full { get; set; }

        public override string ToString()
        {
            return string.Format("id:\n{0}\n", Id) +
                   string.Format("Tittle:\n{0}\n", Tittle) +
                   string.Format("Date:\n{0}\n", Date) +
                   string.Format("Info:\n{0}\n", Info) +
                   string.Format("Full:\n{0}\n", Full) +
            "imgBytes:\n" + Encoding.Default.GetString(Img).Substring(0, 10) + "...";
        }
        

        private static Object syncLock = new Object();
        private static NewsContext db = null;

  /*      public static void ParseNews()
        {
            db = new NewsContext();
            var files = Directory.GetFiles("");

            Parallel.For(0, files.Length, new ParallelOptions { MaxDegreeOfParallelism = 16}, i =>
            {
                parseFile(files[i]);
            });
        }*/

        public static void parseSite(string @from, DateTime last)
        {
  
           
            using (MyWebClient client = new MyWebClient())
            {
                client.Encoding=Encoding.UTF8;
                var html = client.DownloadString(from);
                CQ dom = new CQ(html);
                var links = dom["#content > div > div > div > div > a.bLink"];
                List<String> hrefs = new List<String>();
                foreach (var link in links)
                {
                    var href = link.Attributes["href"];
                   hrefs.Add(href);
                }

                hrefs = hrefs.Distinct().ToList();
                hrefs.Reverse();
                Console.WriteLine("links count: "+hrefs.Count);

                for (int i = 0; i < hrefs.Count;i++)
                {
                    var href = hrefs[i];
                    News model = null;
                    try
                    {
                        db = new NewsContext();
                     //   Console.WriteLine("try "+i);
                        model = SelectModelUrl(href);
          
                        if (model != null && model.check())
                        {
                            if(model.Date <= last)
                                continue;
                            db.news.Add(model);
                            db.SaveChanges();
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine(i+": "+model.Date + " " + model.Tittle);
                        }

                    }
                    catch (DbUpdateException e)
                    {
                        db = new NewsContext();
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("db update ex");
                        Console.WriteLine(e.InnerException.InnerException.Message);
                    }
                    catch (Exception e)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(e.Message+"\n"+model);
                    }
                }
            }
           

        }
 /*       
        private static void parseFile(string file)
        {
            var name = new FileInfo(file).Name;
            var html = File.ReadAllText(file);
            News news = null;
            try
            {

                news = SelectModel(html);
                db = new NewsContext();

                if (news != null && news.check())
                {
                    lock (syncLock)
                    {
                        db.news.Add(news);
                        db.SaveChanges();
                    }
                    Console.WriteLine(name + " ok");
                }
            }
            catch (DbUpdateException ex)
            {
                lock (syncLock)
                {
                    db = new NewsContext();
                }
                Console.WriteLine("DB ex " + ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Bad ex" + ex.Message);
            }
        }
        */
        private bool check()
        {
            if (Tittle == null)
                return false;
            if (Img == null)
                return false;
            if (Full == null)
                return false;
            if (Date == DateTime.MinValue)
                return false;
            return true;
        }
        /*

        private static News SelectModel(string html)
        {
            News item = new News();
            using (MyWebClient client = new MyWebClient())
            {
                CQ dom = new CQ(html);

                var tit1 = dom["#content > div > div.bb.js-mediator-article > h1 > span"].Text();
                var tit2 = dom["#content > div > div.inner-block > div > h1 > span"].Text();

                if (!tit1.Equals(""))
                    item.Tittle = tit1.Trim().Split('\n')[0];
                else if (!tit2.Equals(""))
                    item.Tittle = tit2.Trim().Split('\n')[0];

                var date1 = dom["#content > div > div.bb.js-mediator-article > div.date"].Text();
                var date2 = dom["#content > div > div.inner-block > div > div.date"].Text();

                if (!date1.Equals(""))
                    item.Date = DateTime.Parse(date1.Trim().Split('\n')[0]);
                else if (!date2.Equals(""))
                    item.Date = DateTime.Parse(date2.Trim().Split('\n')[0]);


                var img = dom["img.thumb"].Attr("src");
                if (img.Equals(""))
                    return null;
                try
                {
                    item.Img = client.DownloadData(img);
                }
                catch (Exception)
                {
                    try
                    {
                        item.Img = client.DownloadData(img);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                var full = dom["#item-news-canvas > div.text"].Text().Trim();

                if (full.Equals(""))
                    return null;
                item.Full = full;
                if (item.Full.Length > 200)
                    item.Info = item.Full.Substring(0, 200) + "...";
                else
                    item.Info = item.Full;
                return item;
            }
        }*/
        private static News SelectModelUrl(string url)
        {
            News item = new News();
            using (MyWebClient client = new MyWebClient())
            {
                client.Encoding=Encoding.UTF8;
                var html = client.DownloadString(url);

                CQ dom = new CQ(html);

                var tit1 = dom["#content > div > div.bb.js-mediator-article > h1 > span"].Text();
                var tit2 = dom["#content > div > div.inner-block > div > h1 > span"].Text();

                if (!tit1.Equals(""))
                    item.Tittle = tit1.Trim().Split('\n')[0];
                else if (!tit2.Equals(""))
                    item.Tittle = tit2.Trim().Split('\n')[0];

                //optimization
                if (item.Tittle.Contains("RZN.info") || item.Tittle.ToLower().Contains("фото") || item.Tittle.ToLower().Contains("видео"))
                    return null;
                //end

                var date1 = dom["#content > div > div.bb.js-mediator-article > div.date"].Text().Trim().Split('\n')[0];
                var date2 = dom["#content > div > div.inner-block > div > div.date"].Text().Trim().Split('\n')[0];
                var date3 = dom["#content > div > div.bb.js-mediator-article > div.date.current"].Text().Trim().Split('\n')[0];

                var format = "HH:mm, dd MMMM yyyy";
                var cult = CultureInfo.GetCultureInfo("ru-RU");

                try
                {
                    if (!date1.Equals(""))
                        item.Date = DateTime.ParseExact(date1, format, cult);
                    else if (!date2.Equals(""))
                        item.Date = DateTime.ParseExact(date2, format, cult);
                    else if (!date3.Equals(""))
                        item.Date = DateTime.ParseExact(date3, format, cult);
                }
                catch
                {
                    try
                    {
                        format = "h:mm, dd MMMM yyyy";
                        if (!date1.Equals(""))
                            item.Date = DateTime.ParseExact(date1, format, cult);
                        else if (!date2.Equals(""))
                            item.Date = DateTime.ParseExact(date2, format, cult);
                        else if (!date3.Equals(""))
                            item.Date = DateTime.ParseExact(date3, format, cult);
                    }
                    catch
                    {
                        try
                        {
                            format = "h:mm, d MMMM yyyy";
                            if (!date1.Equals(""))
                                item.Date = DateTime.ParseExact(date1, format, cult);
                            else if (!date2.Equals(""))
                                item.Date = DateTime.ParseExact(date2, format, cult);
                            else if (!date3.Equals(""))
                                item.Date = DateTime.ParseExact(date3, format, cult);
                        }
                        catch
                        {
                            try
                            {
                                format = "HH:mm, d MMMM yyyy";
                                if (!date1.Equals(""))
                                    item.Date = DateTime.ParseExact(date1, format, cult);
                                else if (!date2.Equals(""))
                                    item.Date = DateTime.ParseExact(date2, format, cult);
                                else if (!date3.Equals(""))
                                    item.Date = DateTime.ParseExact(date3, format, cult);
                            }
                            catch
                            {
                                Console.WriteLine("!!!! passer exeption !!!!!");
                                Console.WriteLine(date1);
                                Console.Beep(10000, 300);
                                Console.Read();
                            }
                        }
                    }
                }
    

                var img = dom["img.thumb"].Attr("src");
                if (img.Equals(""))
                    return null;
                try
                {
                    item.Img = client.DownloadData(img);
                }
                catch (Exception)
                {
                    try
                    {
                        item.Img = client.DownloadData(img);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }

                var full = dom["#item-news-canvas > div.text"].Text().Trim();

                if (full.Equals(""))
                    return null;
                item.Full = full;
                if (item.Full.Length > 200)
                    item.Info = item.Full.Substring(0, 200) + "...";
                else
                    item.Info = item.Full;
                return item;
            }
        }
    }
}

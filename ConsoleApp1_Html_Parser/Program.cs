﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
namespace ConsoleApp1_Html_Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("app start");
            Random r = new Random();
            while (true)
            {
                update();

                TimeSpan time;
                var curr = DateTime.Now;
                var start = new DateTime(curr.Year, curr.Month, curr.Day, 8, 0, 0);
                var end = new DateTime(curr.Year, curr.Month, curr.Day, 22, 0, 0);
                var test = curr;
                
                if (test >= start && test < end)
                    time = TimeSpan.FromMinutes(20+r.Next(10,30)); //~30 min
                else
                    time = TimeSpan.FromTicks(start.AddDays(1).Ticks - test.Ticks);

                var next = test.Add(time);
                    Console.WriteLine("> sleep for " + time.Hours+":"+time.Minutes+":"+time.Seconds+" ...");
                    Console.WriteLine("> next run at "+next);

                int sleep = (int) time.Duration().TotalMilliseconds;
                    
                Thread.Sleep(sleep);
            }
        }
        static void update()
        {
            var date = GetLastDate();//new DateTime(2017, 10, 18);
            var end = DateTime.Now;//new DateTime(2017, 10, 10);
            while (date<=end)
            {
               // Console.Beep();
                Console.WriteLine(date);
                News.parseSite(string.Format("https://www.rzn.info/news/{0}/{1}/{2}/", date.Year, date.Month, date.Day),date);
                date = date.AddDays(1);
                date = date.Subtract(date.TimeOfDay);
            }
              
        }

        private static DateTime GetLastDate()
        {
            var context = new NewsContext();
            var d = context.news.Max(x=>x.Date);
            return d;
        }
    }
}
